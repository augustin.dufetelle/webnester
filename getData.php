<?php
// Connexion à la base de données
$pdo = new PDO('mysql:host=your_host;dbname=harvester', 'epsi', 'epsiarras!62');


// Requête pour récupérer les données des tables
$query = "SELECT ports.valeur, resultats_scan.ip
          FROM ports
          INNER JOIN resultats_scan ON ports.id = resultats_scan.ip";

$stmt = $pdo->query($query);
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Convertir les données en JSON
$jsonData = json_encode($data);

// Envoyer le résultat au client
header('Content-Type: application/json');
echo $jsonData;
?>
